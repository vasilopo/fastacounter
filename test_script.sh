#! /usr/bin/env bash

# Test if the "count_sequences.sh" script is working correctly.
#
# - exits status = 0 if it worked correctly
# - exits status = 1 if it failed 

result=$(./count_sequences.sh example.fasta)

if [ "$result" == "example.fasta: 4" ]; then
    exit 0
else
    exit 1
fi
